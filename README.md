# Pulling Image
```
IMAGEDIR=/work/${USER}/images
mkdir -p $IMAGEDIR
singularity pull --force --dir $IMAGEDIR oras://gitlab-registry.oit.duke.edu/qd33/cee690_final_project:latest
```

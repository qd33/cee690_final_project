---
title: "CEE690_project"
output: pdf_document
date: '2022-12-12'
---


```{r}
library(deSolve)
library(modMax)
library(dplyr)
library(ggplot2)
library(foreach)
library(doParallel)
```


```{r}
source('config.R')
```


```{r}
set.seed(12345)
random_graph = list()
bin_adj_matrix = list()
modularity = list()
p_value_mod = list()
nestedness = list()
p_value_nest = list()
adj_matrix = list()
out = list()
Shannon = list()
lotka_volterra_model = function(t, state, parameters){
  with(as.list(c(state, parameters)),{
    dstate = r * state * (1 - state/k) + state * (interaction_strength %*% state)
    return(list(c(dstate)))
  })
}
times =  seq(0, 60, by = 0.1)
species = paste0('N', 1:species_number)
state = rep(1/species_number, species_number)
names(state) = species
r = rnorm(species_number, mean = 1, sd = 0.1)
k = rnorm(species_number, mean = 3/species_number, sd = 0.3/species_number)

registerDoParallel(cores=cores)
random_graph = foreach (i = 1:parallel) %dopar% {
  as.directed(sample_gnm(species_number, ceiling(species_number*(species_number-1)/2*link), directed = FALSE, loops = FALSE), mode = "mutual")
}
bin_adj_matrix = foreach (i = 1:parallel) %dopar% get.adjacency(random_graph[[i]])
mod = foreach (i = 1:parallel) %dopar% extremalOptimization(bin_adj_matrix[[i]]) #, numRandom = 50)
modularity = foreach (i = 1:parallel, .combine = 'c') %dopar% mod[[i]]$modularity
# p_value_mod = foreach (i = 1:parallel, .combine = 'c') %dopar%ecdf(mod[[i]]$`random modularity values`)(mod[[i]]$modularity)

setwd('FALCON-master/R')
source('PERFORM_NESTED_TEST.R')
nestedness = c()
p_value_nest = c()
for(i in 1:parallel){
  nested = PERFORM_NESTED_TEST(matrix(bin_adj_matrix[[i]], nrow = 20), 1, 1, 'NODF', 4, c(), 1)
  nestedness[i] = nested$Bin_t4$NODF$Measure
  p_value_nest[i] = nested$Bin_t4$NODF$pvalue
  E(random_graph[[i]])$weight = runif(length(E(random_graph[[i]])), min = 0, max = strength/link*2) * sample(c(-1,1), length(E(random_graph[[i]])), replace = T)
  dev.off()
}
adj_matrix = foreach (i = 1:parallel) %dopar% get.adjacency(random_graph[[i]], attr = 'weight')
out = list()
for(i in 1:parallel){
  interaction_strength = as.matrix(adj_matrix[[i]])
  parameters = list(r, interaction_strength, k)
  out[[i]] = ode(y = state, times = times, func = lotka_volterra_model, parms = parameters)
}

foreach (i = 1:parallel) %dopar% {
  pdf(file = file.path(result.dir, paste0(i, '.pdf')))
  plot(log(out[[i]][, species[1]], base = 10), lwd = 2, type = 'l', ylim = c(-6, 1), ylab = 'population', xlab = 'time')
  for(j in 2:length(species)){
    lines(log(out[[i]][, species[j]], base = 10), lwd = 2, col = j)
  }
  dev.off()
}
final_population = foreach (i = 1:parallel) %dopar% out[[i]][nrow(out[[i]]),  2:21]
final_population_lite = foreach (i = 1:parallel) %dopar% final_population[[i]][which(final_population[[i]]>0)]
population_proportion = list()
for(i in 1:parallel){
  population_proportion[[i]] = final_population_lite[[i]]/sum(final_population_lite[[i]])
}
Shannon = foreach (i = 1:parallel, .combine = 'c') %dopar%-sum(population_proportion[[i]] * log(population_proportion[[i]]))

pdf(file = file.path(result.dir, 'correlation.pdf'))
par(mfrow=c(2,2))
# plot(Shannon, p_value_mod)
plot(Shannon, modularity)
plot(Shannon, p_value_nest)
plot(Shannon, nestedness)
dev.off()
```

```{r}
cor.test(Shannon, modularity, method = 'kendall')
# cor.test(Shannon, p_value_mod, method = 'kendall')
cor.test(Shannon, nestedness, method = 'kendall')
cor.test(Shannon, p_value_nest, method = 'kendall')
```

```{r}
cor.test(Shannon, modularity, method = 'spearman')
# cor.test(Shannon, p_value_mod, method = 'spearman')
cor.test(Shannon, nestedness, method = 'spearman')
cor.test(Shannon, p_value_nest, method = 'spearman')
```

```{r}
data = data.frame(cbind(modularity, nestedness, p_value_nest, Shannon)) #p_value_mod,
write.csv(data, file.path(result.dir, 'result.csv'), quote = F, row.names = F)
saveRDS(out, file.path(result.dir, 'raw_data.RData'))
```

